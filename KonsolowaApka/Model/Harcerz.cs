﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonsolowaApka.Model
{ 
    class Harcerz
    {
        public string imie;
        public string nazwisko;
        public Stopien stopien;

        public Harcerz(string imie, string nazwisko, Stopien stopien)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.stopien = stopien;
        }
    }
}
