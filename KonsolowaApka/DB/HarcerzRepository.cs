﻿using KonsolowaApka.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace KonsolowaApka.DB
{
    static class HarcerzRepository
    {
        static string path = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            @"KonsolowAplikacja\");
        static string file = "harcerze.json";
        static string filePath = Path.Combine(path, file);

        static public List<Harcerz> Fetch()
        {
            if (!File.Exists(filePath))
            {
                Directory.CreateDirectory(path);
                File.Create(filePath).Close();
            }
            string json = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<List<Harcerz>>(json);
        }

        static public void Save(Harcerz harcerz)
        {
            List<Harcerz> harcerze = Fetch();
            if (harcerze == null)
            {
                harcerze = new List<Harcerz>();
            }
            harcerze.Add(harcerz);
            string json = JsonConvert.SerializeObject(harcerze);
            File.WriteAllText(filePath, json);
        }

    }
}
