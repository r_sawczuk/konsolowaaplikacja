﻿using KonsolowaApka.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonsolowaApka.Controller
{
    class MainMenuController
    {
        MainMenuView view = new MainMenuView();

        public MainMenuController()
        {
            MainCycle();
        }

        void MainCycle()
        {
            while (true)
            {
                view.ShowView();
                String userInputString = view.getUserInput();
                int userInputInt;
                if (Int32.TryParse(userInputString, out userInputInt))
                {
                    switch (userInputInt)
                    {
                        case 1:
                            new ListaController();
                            break;

                        case 0:
                            return;

                        default:
                            break;
                    }
                }
            }
        }

    }
}
