﻿using KonsolowaApka.DB;
using KonsolowaApka.Model;
using KonsolowaApka.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonsolowaApka.Controller
{
    class ListaController
    {
        ListaView view = new ListaView();

        public ListaController()
        {
            List<Harcerz> harcerze = HarcerzRepository.Fetch();

            view.ShowListWith(harcerze);
        }
    }
}
