﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonsolowaApka.View
{
    class MainMenuView
    {
        public void ShowView()
        {
            Console.Clear();
            Console.WriteLine("MENU");
            Console.WriteLine("1 - Lista");
            Console.WriteLine("0 - Zakończ");
        }

        public string getUserInput()
        {
            Console.Write("-> ");
            return Console.ReadLine();
        }
    }
}
