﻿using KonsolowaApka.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonsolowaApka.View
{
    class ListaView
    {
        public void ShowListWith(List<Harcerz> harcerze)
        {
            Console.Clear();
            Console.WriteLine("List list harcerzy ktorzuy nie wroclili");
            Console.WriteLine("=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-");
            int index = 1;
            foreach (var harcerz in harcerze)
            {
                Console.WriteLine(index++ + " - " + harcerz.imie + " " + harcerz.nazwisko + " - " + harcerz.stopien);
            }
            Console.ReadKey();
        }
    }
}
